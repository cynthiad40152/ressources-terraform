### EC2 Instances
resource "aws_instance" "instance_1" {
  ami           = "${var.ami}"
  instance_type = "t2.micro"
  subnet_id     = "${aws_subnet.subnet_1.id}"
  key_name = "${var.key_name}"
  vpc_security_group_ids = ["${aws_security_group.sg.id}"]
  associate_public_ip_address = "true"

  tags = {
    Name = "instance_1"
  }
}
