### Resources

### VPC
resource "aws_vpc" "vpc_1" {
  cidr_block = "10.0.0.0/16"

  tags {
    Name = "vpc_1"
  }
}

## SUBNET
resource "aws_subnet" "subnet_1" {
  vpc_id                  = "${aws_vpc.vpc_1.id}"
  cidr_block              = "10.0.0.0/24"
  availability_zone       = "eu-west-1a"

  tags {
    Name = "subnet_1"
  }
}

## INTERNET GATEWAY
resource "aws_internet_gateway" "gw" {
  vpc_id = "${aws_vpc.vpc_1.id}"

  tags {
    Name = "gw"
  }
}

## ROUTE TABLE
resource "aws_route_table" "route_table_1" {
  vpc_id = "${aws_vpc.vpc_1.id}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.gw.id}"
  }

  tags {
    Name = "route_table_1"
  }
}

## ROUTE TABLE ASSOCIATION
resource "aws_route_table_association" "RTA_1" {
  subnet_id      = "${aws_subnet.subnet_1.id}"
  route_table_id = "${aws_route_table.route_table_1.id}"
}
