resource "aws_security_group" "sg" {
  name        = "sg"
  vpc_id      = "${aws_vpc.vpc_1.id}"

  ingress {
    from_port = 0
    to_port   = 0
    protocol  = "-1"
    cidr_blocks     = ["${var.myip}"]

  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks     = ["0.0.0.0/0"]

  }
}
